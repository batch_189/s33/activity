fetch ('https://jsonplaceholder.typicode.com/todos', {
    method: 'GET'
})
    .then((Response) => Response.json())
    .then((data) => console.log(data))


// fetch ('https://jsonplaceholder.typicode.com/todos')
//     .then((response) => response.json())
//     .then((data) => {
//         console.log(data.map(toDo => {
//             return toDo.title
//         }));
// });

fetch ('https://jsonplaceholder.typicode.com/todos')
    .then((response) => response.json())
    .then((data) => {
        toDo_titles = data.map((toDo => {
            return toDo.title
        }));
})
.then(() => console.log(toDo_titles));


fetch ('https://jsonplaceholder.typicode.com/todos', {
    method: 'POST',
    headers: {
        'content-Type': 'application/json'
    },
    body: JSON.stringify({
        userId: 201,
        title: "The activity",
        body: "Hooray"

    })
})
    .then((Response) => Response.json())
    .then((data) => console.log(data))


fetch ('https://jsonplaceholder.typicode.com/todos/10')
    .then((Response) => Response.json())
    .then((data) => console.log(data.title))


fetch ('https://jsonplaceholder.typicode.com/todos/10', {
    method: 'PUT',
    headers: {
        'content-Type': 'application/json'
    },
    body: JSON.stringify({
        title: "Postman",
        description: "Activity 33",
        status: "completed",
        dateCompleted: "2022-06-28",
        userId: 202
    })
})
    .then((response) => response.json())
    .then((data) => console.log(data))



fetch ('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PATCH',
    headers: {
        'content-Type': 'application/json'
    },
    body: JSON.stringify ({
        status: 'pending',
        dateStatus: '2022-06-28'
    })
})
    .then((response) => response.json())
    .then((data) => console.log(data))


    fetch ('https://jsonplaceholder.typicode.com/todos/201', {
        method: 'DELETE'
    })
        .then((response) => response.json())
        .then((data) => console.log(data))



    